#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
$ python3 server_sip.py 6001 
"""

import socketserver
import sys

# Constantes. Puerto.
PORT = int(sys.argv[1])

class SIPRequest():

    def __init__(self, data):
        self.data = data

    def parse(self):
        self._parse_command(self.data.decode('utf-8'))

    def _get_address(self, uri):
        bloques = uri.split(':')
        address = bloques[1]
        schema = bloques[0]
        #print(address)
        return address, schema

    def _parse_command(self, line):
        bloques = line.split()
        self.command = bloques[0]
        self.uri = bloques[1]
        #print(self.uri)
        self.address, schema = self._get_address(self.uri)
        if schema != 'sip':
            self.result = '416 Unsupported URI Scheme'
        elif self.command != 'REGISTER':
            self.result = '405 Method Not Allowed'
        else:
            self.result = '200 OK'

    def _parse_headers(self, first_nl):
        pass#inicializar dicc


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """
    #inicializar diccionario
    register = {}

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]
        sip_request = SIPRequest(data)
        sip_request.parse()
        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)
        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)
        print(self.register)

    def process_register(self, request):
        #registra address e ip en un diccionario 
        self.register[request.address] = self.client_address[0]

def main():
    # Listens at port PORT (my address)
    # and calls the SIPRegisterHandler class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"Server listening in port {PORT}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
