#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor 
$ python3 client_sip.py 127.0.0.1 6001 register maria@registrar1.com 3600
"""

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = sys.argv[1]
PORT = int(sys.argv[2])
REG = sys.argv[3]
DIR = sys.argv[4]
EXP_VAL = sys.argv[5]


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    
            LINE = f"REGISTER sip:{DIR} SIP/2.0\r\nExpires: {EXP_VAL}\r\n"
            print(LINE)
            my_socket.sendto(LINE.encode('utf-8'), (SERVER, PORT))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
           
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
